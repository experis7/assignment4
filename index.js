//Initializing global variables that holds the values for work balance, bank balance and the owed amount.
let workBalance= 0
let bankBalance = 0
let owedAmount = 0

// Storing DOM elements in constants.
const repayLoanButton = document.getElementById("repayLoanButton")
const loanButton = document.getElementById("loanButton")
const workBalanceDisplay = document.getElementById("displayWorkBalance") 
const bankWorkMoneyButton = document.getElementById("bankWorkMoneyButton")
const bankBalanceDisplay = document.getElementById("bankBalanceDisplay")
const owedAmountDisplay = document.getElementById("owedAmountDisplay")
const workButton = document.getElementById('workButton')
//Adding listener to the button and calls the handle function when its clicked.
bankWorkMoneyButton.onclick = function(){
    bankWorkMoney()
}
//Adding listener to work button that adds 100 to the workBalance and calls a function that updates the display.
workButton.onclick = function(){
    workBalance += 100
    displayWorkBalance()
}
//Listener to the loanButton. Calls the function that handles loan apply.
loanButton.onclick = function(){
    applyForLoan()
}
//Listener to replay loan button. Calls a function to handle the request.
repayLoanButton.onclick = function(){
    repayLoan()
}
//Function to handle a repay loan request. Checks if the bank balance is enough to repay the entire loan, if it is, the button and text related to the loan will disappear and the loan amount will be subtracted from the bank balance. If the bank balance is not greater or equal to the loan amount, the entire bank balance will be subtracted from the loan balance. The button and text with the loan information will remain and the new bank balance will be set to zero. The function calls the update display functions to update the display to the end user.
function repayLoan(){
    if(bankBalance >= owedAmount){
        bankBalance -= owedAmount
        owedAmount = 0
        displayBankBalance()
        displayOwedAmount()
        repayLoanButton.style.visibility = "hidden"
    }
    else{
        owedAmount -= bankBalance
        bankBalance = 0
        displayBankBalance()
        displayOwedAmount()
    }
}

//Function to bank the work money. Checks if there is an owed amount. If it is, 10% of the banked amount will be used to repay the loan. The remaining 90% will be added to the bank balance. The update display functions will update the balances to the end user.
function bankWorkMoney(){
    if(owedAmount !== 0){
        var amountToPayLoan = workBalance/10
        workBalance -= amountToPayLoan
        owedAmount -= amountToPayLoan
    }
    
    bankBalance += workBalance
    workBalance = 0
    displayWorkBalance()
    displayBankBalance()
    displayOwedAmount()
}
//Function to update the text inside the DOM for displaying the bank balance
function displayBankBalance(){
    bankBalanceDisplay.innerText = bankBalance + " NOK"
}
//Function to update the text inside the DOM for displaying the work balance
function displayWorkBalance(){
    workBalanceDisplay.innerText = workBalance + " NOK"
}
//Function to display the amount of owed money. Checks if there exist a loan for the user. If there is not, all elements related to the loan will be set to hidden. 
function displayOwedAmount(){
    if(owedAmount > 0){
        owedAmountDisplay.innerText = "Owed amount:   " + owedAmount +" NOK"
    }
    else{
        owedAmountDisplay.innerText = ""
        repayLoan. visibility = "hidden"
    }
}
//Function to handle a request for a loan. First it checks if the user already have a loan. The user will get an alert telling them to repaying it before applying for a new one. If the user do not have a loan, a prompt will be given for the user to enter the wanted amount of money. If the loan amount is more than 50% of the bank balance, an alert will be given. If the input is not a numeric value, an alert will also be given.
function applyForLoan(){
    if(owedAmount > 0){
        window.alert("You already got a loan. Please repay it before you apply for a new one.")
        return;
    }
    let wishedLoanAmount = Number(window.prompt("Please enter the amount you wish to loan"))
    if(isNaN(wishedLoanAmount)){
        window.alert("Please enter a valid number!")
        applyForLoan()
    }
    else if(bankBalance/2 < wishedLoanAmount) {
        window.alert("You need more balance in your account to loan that amount")
        applyForLoan()
    }
    else{
        owedAmount = wishedLoanAmount
        bankBalance += owedAmount
        displayBankBalance()
        displayOwedAmount()
        repayLoanButton.style.visibility = "visible"
    }
}

//Initialize the DOMs that will hold the values from the json response
const computersElement = document.getElementById("computers")
const computerSelectedTitle= document.getElementById("computerTitle")
const computerSelectedDescription = document.getElementById("computerDescription")
const computerSelectedPrice = document.getElementById("computerPrice")
const computerSelectedFeatures = document.getElementById("computerFeatures")
let computers = []
const computerSelectedImage = document.getElementById("computerPicture")
const buySelectedComputerButton = document.getElementById("buyComputerButton")

//Fetch data from the API
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers))

//Looping through the computers and sending each computer to a handle function. Adding the information of the first computer of the list to all the DOMs to display the information.
const addComputersToList = (computers) =>{
    computers.forEach(computer => {
        addComputerToList(computer)
    });
    computerSelectedDescription.innerText = computers[0].description
    computerSelectedTitle.innerText = computers[0].title
    computerSelectedPrice.innerText = computers[0].price + " NOK"
    computerSelectedImage.src = "https://hickory-quilled-actress.glitch.me/" + computers[0].image
    computers[0].specs.forEach(x =>{
        computerSelectedFeatures.innerText += x +"\n"
    });
}
//Adding childs to the dropdown menu.
const addComputerToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

//Updating the DOMs to match the information of the selected computer.
const handleComputerListChange = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    computerSelectedTitle.innerText = selectedComputer.title
    computerSelectedDescription.innerText = selectedComputer.description
    computerSelectedPrice.innerText = selectedComputer.price + " NOK"
    computerSelectedFeatures.innerText = ""
    for(let feature in selectedComputer.specs){
        computerSelectedFeatures.innerText += selectedComputer.specs[feature] + "\n" 

    }
    computerSelectedImage.src = "https://hickory-quilled-actress.glitch.me/"+selectedComputer.image

}
//Adding listener to the dropdown.
computersElement.addEventListener("change",handleComputerListChange)
//Adding listener to the buy button.
buySelectedComputerButton.addEventListener("click",handleBuyButtonClick)

//Function to handle the buy button click. Checks if the bank balance is greater or equal to the computer price and if the computer stock is greater than zero. If these requirements are met, the computer price will be subtracted from the bank balance and the balance will be updated. An alert will be given with information of the bought computer.
function handleBuyButtonClick(){
    const selectedComputer = computers[computersElement.selectedIndex]
    const stockQuantity = selectedComputer.stock
    console.log(stockQuantity)
    console.log(selectedComputer.price)
    console.log(bankBalance)
    if(selectedComputer.price > bankBalance){
        window.alert("You cannot afford that")
    }
    else if(stockQuantity === 0){
        window.alert("The item is out of stock")
    }
    else{
        bankBalance -= parseInt(selectedComputer.price)
        window.alert(`Congratulation! You successfully bought a ${selectedComputer.title}`)
        displayBankBalance()
    }
}