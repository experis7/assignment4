# Assignment4
This web app is built using HTML,CSS and vanilla Javascript. The web app is hosted by using Microsoft Azure, and it can be found at [https://assignment4sondreeftedal.azurewebsites.net/].


## App Description
The app has a work button that gives the user 100 "NOK" each time the button is pressed. There is also a bank button where the user can add the work money to the bank balance. A user can apply for a loan by clicking the "Apply for loan" button. When the user has an active loan, the amount left on the loan and a repay loan button will be added to the page. The information about the products is fetched from an API.

### Rules
The App has a few rules:
* A user can only have one active loan. A loan needs to be fully repaid before a user can apply for a new one.
* If the user has an active loan, each time the user banks the work money, 10% will automatically be used as a down payment for the loan. 
* A loan will not be accepted if it exceeds 50% of the amount of money in the bank.
* When a user clicks the repay loan button, the whole loan will automatically be repaid. If the bank account does not have enough money to repay the whole loan, all the money in the account will be used as a down payment. 
* When a user tries to buy a computer, the user must have enough money in the bank account. The app also checks if the computer is in stock, but we do not have an endpoint to update the stock when an order is placed.
